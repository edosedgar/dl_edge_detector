# coding: utf-8

import numpy as np
import imageio
import sys
import time
import torchvision.transforms.functional as TF
from model import UNet

# Prepare the network
net = UNet(weights='./weights/fedge_detector_nuc_unet.pth.tar')

# Load image from arguments
image = TF.to_tensor(imageio.imread(sys.argv[1])).float().unsqueeze_(0)
start = time.time()
mask = net.eval_predict(image)
end = time.time()
print("Time taken:", end-start)
imageio.imwrite("output.png", mask.astype(np.uint8))
